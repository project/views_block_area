# Views block area

Views block area module exposes all available blocks as a views area or field. You can also add any block inside a field, footer or header in your view.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/views_block_area).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/views_block_area).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This module has no menu or modifiable settings. There is no configuration. When
enabled, the module will disable the core administrator roles form. To restore
the core functionality, disable the module and clear caches.


## How it works

### Add a block as an area

#### Steps -

1. Go to the view that needs a block inside an area
2. Click on add next to header, footer or no result behavior
3. Select Global: Block area
4. Select the block that you want

After saving the view, the block will now be visible in the header or footer.
